//
//  ViewController.swift
//  Homework-1
//
//  Created by Иван Таптунов on 16.03.24.
//

/// Экран 1. Запуск приложения

import UIKit

class ViewController: UIViewController {
    
    // MARK: - lifecycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - properties
        
        //скармливаем вводные. причем задаю явно тип данных дробный, что бы при изменении какого-либо числа на входе на целое, не посыпался алгоритм
        
        let inputDataA, inputDataB, inputDataC, inputDataD : Double
        inputDataA = 2.5
        inputDataB = 9.7
        inputDataC = 6.9
        inputDataD = 8.2
        
        //получаем данные с целыми числами из вводных
        
        let outIntA = Int(inputDataA)
        let outIntB = Int(inputDataB)
        let outIntC = Int(inputDataC)
        let outIntD = Int(inputDataD)
        
        //получаем дробные сначала надо перевести полученные целые в дробные, что бы можно было работать с парами данных
        
        let outIntToFloatA = Double(outIntA)
        let outIntToFloatB = Double(outIntB)
        let outIntToFloatC = Double(outIntC)
        let outIntToFloatD = Double(outIntD)
        
        //математика по получению дробных
        
        let outFlA = inputDataA - outIntToFloatA
        let outFlB = inputDataB - outIntToFloatB
        let outFlC = inputDataC - outIntToFloatC
        let outFlD = inputDataD - outIntToFloatD
        
        // решение с целыми
        
        let solutionInt = outIntA + outIntB + outIntC + outIntD
        
        // решение с дробными
        
        let solutionFloat = outFlA + outFlB + outFlC + outFlD
        
        //печать
        
        print (solutionInt)
        print (NSString(format:"%.1f",solutionFloat))
        print(Float(solutionFloat))
    }
}
